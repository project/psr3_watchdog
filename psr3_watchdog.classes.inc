<?php

use Psr\Log\LogLevel;
use Psr\Log\AbstractLogger;

class Psr3Watchdog extends AbstractLogger {

  static $level_map = array(
    LogLevel::EMERGENCY => WATCHDOG_EMERGENCY,
    LogLevel::ALERT => WATCHDOG_ALERT,
    LogLevel::CRITICAL => WATCHDOG_CRITICAL,
    LogLevel::ERROR => WATCHDOG_ERROR,
    LogLevel::WARNING => WATCHDOG_WARNING,
    LogLevel::NOTICE => WATCHDOG_NOTICE,
    LogLevel::INFO => WATCHDOG_INFO,
    LogLevel::DEBUG => WATCHDOG_DEBUG,
  );

  private $type = 'PSR-3';

  /**
   * Sets the type of watchdog entries created by this Psr3Watchdog instance.
   * If not set, 'PSR-3' is used.
   *
   * @param string $type
   *   The category to which this message belongs. Can be any string, but
   *   the general practice is to use the name of the module calling watchdog().
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Logs with an arbitrary level.
   *
   * @param mixed $level
   * @param string $message
   * @param array $context
   * @return null
   */
  public function log($level, $message, array $context = array()) {
    $watchdog_level = static::$level_map[$level];
    if (variable_get('psr3_watchdog_min_severity', WATCHDOG_WARNING) >= $watchdog_level) {
      // Every method accepts an array as context data. This is meant to hold
      // any extraneous information that does not fit well in a string.
      // The array can contain anything. Implementors MUST ensure they treat
      // context data with as much lenience as possible. A given value in the
      // context MUST NOT throw an exception nor raise any php error, warning
      // or notice.
      // If an Exception object is passed in the context data, it MUST be in
      // the 'exception' key. Logging exceptions is a common pattern and this
      // allows implementors to extract a stack trace from the exception when
      // the log backend supports it. Implementors MUST still verify that the
      // 'exception' key is actually an Exception before using it as such, as
      // it MAY contain anything.
      // @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md#13-context
      if (isset($context['exception']) && is_object($context['exception']) && is_a($context['exception'], 'Exception')) {
        watchdog(
          $this->type,
          '@exception',
          array('@exception' => (string) $context['exception']),
          $watchdog_level
        );

        unset($context['exception']);
      }

      // Every method accepts a string as the message, or an object with a
      // __toString() method. Implementors MAY have special handling for the
      // passed objects. If that is not the case, implementors MUST cast it to
      // a string.
      // @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md#12-message
      $message = (string) $message;
      $context_message = array();
      $variables = array();
      foreach ($context as $key => $value) {
        // If $value is an array, encode it as a JSON string.
        if (is_array($value)) {
          $value = json_encode($value);
        }
        // $value could be a string or an object  with a __toString() method
        $variables['@' . $key] = (string) $value;

        if (strpos($message, '{' . $key . '}') !== FALSE) {
          // Convert PSR-3 placeholder to drupal placeholder
          $message = str_replace('{' . $key . '}', '@' . $key, $message);
        }
        else {
          $context_message[] = check_plain($key) . ': @' . $key;
        }
      }

      if (!empty($context_message)) {
        $message .= ' (' . implode(', ', $context_message) . ')';
      }

      watchdog(
        $this->type,
        $message,
        $variables,
        $watchdog_level
      );
    }
  }
}
